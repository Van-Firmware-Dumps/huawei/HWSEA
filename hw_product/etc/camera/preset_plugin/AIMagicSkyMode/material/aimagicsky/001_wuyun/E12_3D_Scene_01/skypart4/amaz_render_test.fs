#extension GL_OES_standard_derivatives : enable // android compile error oes texture
//
precision mediump float;

// material
uniform vec3 ambient;           //Emission Color
uniform vec3 diffuse;           //if no diffuseMap, using diffuseColor
uniform vec3 specular;          //if no specularMap ,using specular .

uniform float glossy;        //if use cook torrance .it represent roughness

uniform float ka;
uniform float ke;
uniform float ks;
uniform float kd;
uniform float kr;
uniform float alpha;

// sampler
#ifdef USE_DIFFUSE_MAP
uniform sampler2D diffuseMap;
#endif

#ifdef USE_NORMAL_MAP
uniform sampler2D normalMap;
#endif

#ifdef USE_EMISSION_MAP
uniform sampler2D emissionMap;
#endif

#ifdef USE_SPECULAR_MAP
uniform sampler2D specularMap;
#endif

#ifdef USE_GLOSSINESS_MAP
uniform sampler2D glossinessMap;
#endif

#ifdef USE_OPACITY_MAP
uniform sampler2D opacityMap;
#endif

#ifdef USE_REFLECTION_MAP
uniform sampler2D reflectionMap;
#endif

#ifdef USE_Environment_MAP
uniform sampler2D environmentMap;
#endif

// varying
varying vec3      N;
varying vec3      T;
varying vec2      textureCoords;
varying vec3      fragPos;
varying vec3      fragNormal;
//in vec3      N;
//in vec3      T;
//in vec2      textureCoords;
//in vec3      fragPos;
//in vec3      fragNormal;


// uniform
uniform vec3 viewPos;

uniform float uOffset; // uv animation
uniform float vOffset;
uniform float uvColumnNum;
uniform float uvRowNum;

uniform float envRotationX;
uniform float envRotationY;

const int MAX_POINT_LIGHT = 2;
const int MAX_SPOT_LIGHT = 2;
const int MAX_DIR_LIGHT = 2;

//dir light
uniform vec3 dirLightDirection[MAX_DIR_LIGHT];
uniform vec3 dirLightColor[MAX_DIR_LIGHT];
uniform float dirLightIntensity[MAX_DIR_LIGHT];

//point light
uniform vec3 pointLightPosition[MAX_POINT_LIGHT];
uniform float pointLightRadius[MAX_POINT_LIGHT];
uniform vec3 pointLightColor[MAX_POINT_LIGHT];
uniform float pointLightIntensity[MAX_POINT_LIGHT];

//spot light
uniform vec3 spotLightPosition[MAX_SPOT_LIGHT];
uniform float spotLightRadius[MAX_SPOT_LIGHT];
uniform vec3 spotLightColor[MAX_SPOT_LIGHT];
uniform float spotLightIntensity[MAX_SPOT_LIGHT];
uniform vec2 spotLightInout[MAX_SPOT_LIGHT];
uniform vec3 spotLightDirection[MAX_SPOT_LIGHT];

// ambient light
uniform vec3 ambientLight;

// function
vec2 getUVFromEvMap(vec3 direction)
{
    direction = normalize(direction);
    float x = direction.x;
    float y = direction.y;
    float z = direction.z;

    float p = acos(direction.y);
    float h = atan(direction.x, direction.z);

    float pi = 3.1415926535898;
    float inversePi = 0.31830988618379;
    if (x >= 0.0) {
        h = h;
    }
    else {
        h = 2.0 * pi + h;
    }
    float v = p * inversePi;
    float u = h * inversePi * 0.5;

    return vec2(u, v);
}

vec3 rotateEvMap(vec3 v, float h, float p)
{
    float ca = cos(h);
    float sa = sin(h);
    v = vec3(v.x*ca+v.z*sa, v.y, v.z*ca-v.x*sa);

    ca = cos(p);
    sa = sin(p);

    v = vec3(v.x, ca*v.y+sa*v.z, ca*v.z-sa*v.y);
    return v;
}

vec3 calNormalMap(vec3 texNormal,vec3 N,vec3 T)
{
    vec3 normal = normalize(N);
    vec3 tangent = normalize(T);

    tangent = normalize(tangent - dot(tangent, normal) * normal);
    vec3 bitangent = normalize(cross(tangent, normal));
    texNormal = 2.0 * texNormal - vec3(1.0, 1.0, 1.0);

    vec3 newNormal;
    mat3 TBN = mat3(tangent, bitangent, normal);
    newNormal = TBN * texNormal;
    newNormal = normalize(newNormal);
    return newNormal;
}


void blinPhongShading(vec3 Ln,
                      vec3 Vn,
                      vec3 Nn,
                      float glossiness,
                      out vec3 DiffuseContrib,
                      out vec3 SpecularContrib)
{
    float inversePi = 0.31830988618379;
    vec3 Hn = normalize(Ln+Vn);
    float ldn = dot(Nn, Ln);
    ldn = max(ldn, 0.0);
    float ndh = max(dot(Nn, Hn), 0.0);

    float specPow = exp2(glossiness*11.0 + 2.0);
    DiffuseContrib = vec3(ldn);
    float specNorm = (specPow + 8.0) / 8.0;
    SpecularContrib = vec3(specNorm * pow(ndh, specPow) * ldn);

    //DiffuseContrib *= inversePi;
    //pecularContrib *= inversePi;
}

void calPointLight(vec3 lightPos,
                   vec3 eyePos,
                   vec3 fragPos,
                   vec3 Nn,
                   float glossiness,
                   float radius,
                   out vec3   DiffuseContrib,
                   out vec3   SpecularContrib)
{
    vec3 Ln = normalize(lightPos - fragPos);
    vec3 Vn = normalize(eyePos - fragPos);
    float distance = length(lightPos - fragPos);
    float inverseRadius = 1.0 / radius;
    //float attenuation  = 1 - max(1.0 - pow(pow(distance * inverseRadius, 4.0),2.0), 0.0) / (pow(distance, 2.0) + 1.0);
    float attenuation = 1.0;

    blinPhongShading(Ln, Vn, Nn, glossiness, DiffuseContrib, SpecularContrib);
    DiffuseContrib *= attenuation;
    SpecularContrib *= attenuation;
}

void calSpotLight( vec3 lightPos,
                  vec3 eyePos,
                  vec3 fragPos,
                  vec3 direction,
                  vec3 Nn,
                  float glossiness,
                  float innerAngle,
                  float outAngle,
                  float radius,
                  out vec3   DiffuseContrib,
                  out vec3   SpecularContrib)
{
    vec3 Ln = normalize(lightPos - fragPos);
    vec3 Vn = normalize(eyePos - fragPos);
    float distance = length(lightPos - fragPos);
    float inverseRadius = 1.0 / radius;
    //    float attenuation  = max(1.0 - pow(pow(distance * inverseRadius, 4.0),2.0), 0.0) / (pow(distance, 2.0) + 1.0);
    float attenuation = 1.0;

    blinPhongShading(Ln, Vn, Nn, glossiness, DiffuseContrib, SpecularContrib);

    float theta = dot(-Ln, normalize(direction));
    float epsilon = innerAngle - outAngle;
    float intensity = clamp((theta - outAngle) / epsilon, 0.0, 1.0);

    DiffuseContrib = DiffuseContrib * attenuation * intensity;
    SpecularContrib = SpecularContrib * attenuation * intensity;
}

void calDirLight(vec3 Ln,
                 vec3 eyePos,
                 vec3 fragPos,
                 vec3  Nn,
                 float glossiness,
                 out vec3   DiffuseContrib,
                 out vec3   SpecularContrib)
{
    vec3 Vn = normalize(eyePos - fragPos);
    blinPhongShading(Ln, Vn, Nn, glossiness, DiffuseContrib, SpecularContrib);
}


// main
void main()
{
    float uOffsetStep = 1.0 / uvColumnNum;
    float vOffsetStep = 1.0 / uvRowNum;

    vec2 processedTextureCoords = textureCoords;
    processedTextureCoords = vec2(textureCoords.x / uvColumnNum + uOffsetStep * uOffset,
                                  textureCoords.y / uvRowNum + vOffsetStep * vOffset);
    gl_FragColor = texture2D(diffuseMap, processedTextureCoords);
}
